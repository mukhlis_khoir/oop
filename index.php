<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new Animal ("shaun");

echo "Nama : " . $sheep->name ."<br>"; // "shaun"
echo "Jumlah kaki : " . $sheep->legs."<br>"; // 4
echo "Darah dingin: " . $sheep->cold_blooded."<br><br>"; // "no"


$frog = new frog ("kodok");

echo "Nama : " . $frog->name ."<br>"; // "shaun"
echo "Jumlah kaki : " . $frog->legs."<br>"; // 4
echo "Darah dingin: " . $frog->cold_blooded."<br>"; // "no"
echo "Jump: " . $frog->jump."<br><br>"; // "no"

$ape = new ape ("kera");

echo "Nama : " . $ape->name ."<br>"; // "shaun"
echo "Jumlah kaki : " . $ape->legs."<br>"; // 4
echo "Darah dingin: " . $ape->cold_blooded."<br>"; // "no"
echo "Yell: " . $ape->yell."<br><br>"; // "no"

echo $ape->yell("Auoo")

?>